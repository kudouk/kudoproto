// Code generated by protoc-gen-go. DO NOT EDIT.
// source: supplier.d.proto

package proto

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type CompanyAddress struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	CompanyId            int64    `protobuf:"varint,2,opt,name=companyId,proto3" json:"companyId,omitempty"`
	AddressLine1         string   `protobuf:"bytes,3,opt,name=AddressLine1,proto3" json:"AddressLine1,omitempty"`
	AddressLine2         string   `protobuf:"bytes,4,opt,name=AddressLine2,proto3" json:"AddressLine2,omitempty"`
	AddressCity          string   `protobuf:"bytes,5,opt,name=AddressCity,proto3" json:"AddressCity,omitempty"`
	AddressZip           string   `protobuf:"bytes,6,opt,name=AddressZip,proto3" json:"AddressZip,omitempty"`
	AddressCountry       string   `protobuf:"bytes,7,opt,name=AddressCountry,proto3" json:"AddressCountry,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CompanyAddress) Reset()         { *m = CompanyAddress{} }
func (m *CompanyAddress) String() string { return proto.CompactTextString(m) }
func (*CompanyAddress) ProtoMessage()    {}
func (*CompanyAddress) Descriptor() ([]byte, []int) {
	return fileDescriptor_1c26ca21821219d0, []int{0}
}

func (m *CompanyAddress) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CompanyAddress.Unmarshal(m, b)
}
func (m *CompanyAddress) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CompanyAddress.Marshal(b, m, deterministic)
}
func (m *CompanyAddress) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CompanyAddress.Merge(m, src)
}
func (m *CompanyAddress) XXX_Size() int {
	return xxx_messageInfo_CompanyAddress.Size(m)
}
func (m *CompanyAddress) XXX_DiscardUnknown() {
	xxx_messageInfo_CompanyAddress.DiscardUnknown(m)
}

var xxx_messageInfo_CompanyAddress proto.InternalMessageInfo

func (m *CompanyAddress) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *CompanyAddress) GetCompanyId() int64 {
	if m != nil {
		return m.CompanyId
	}
	return 0
}

func (m *CompanyAddress) GetAddressLine1() string {
	if m != nil {
		return m.AddressLine1
	}
	return ""
}

func (m *CompanyAddress) GetAddressLine2() string {
	if m != nil {
		return m.AddressLine2
	}
	return ""
}

func (m *CompanyAddress) GetAddressCity() string {
	if m != nil {
		return m.AddressCity
	}
	return ""
}

func (m *CompanyAddress) GetAddressZip() string {
	if m != nil {
		return m.AddressZip
	}
	return ""
}

func (m *CompanyAddress) GetAddressCountry() string {
	if m != nil {
		return m.AddressCountry
	}
	return ""
}

type Company struct {
	Id                   int64           `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string          `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Website              string          `protobuf:"bytes,3,opt,name=website,proto3" json:"website,omitempty"`
	Phone                string          `protobuf:"bytes,4,opt,name=phone,proto3" json:"phone,omitempty"`
	Email                string          `protobuf:"bytes,5,opt,name=email,proto3" json:"email,omitempty"`
	WhiteLabel           bool            `protobuf:"varint,6,opt,name=whiteLabel,proto3" json:"whiteLabel,omitempty"`
	ApplicationNotes     string          `protobuf:"bytes,7,opt,name=applicationNotes,proto3" json:"applicationNotes,omitempty"`
	CompanyNumber        string          `protobuf:"bytes,8,opt,name=companyNumber,proto3" json:"companyNumber,omitempty"`
	CompanyAddress       *CompanyAddress `protobuf:"bytes,9,opt,name=companyAddress,proto3" json:"companyAddress,omitempty"`
	State                string          `protobuf:"bytes,10,opt,name=state,proto3" json:"state,omitempty"`
	CreatedAt            string          `protobuf:"bytes,11,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	XXX_NoUnkeyedLiteral struct{}        `json:"-"`
	XXX_unrecognized     []byte          `json:"-"`
	XXX_sizecache        int32           `json:"-"`
}

func (m *Company) Reset()         { *m = Company{} }
func (m *Company) String() string { return proto.CompactTextString(m) }
func (*Company) ProtoMessage()    {}
func (*Company) Descriptor() ([]byte, []int) {
	return fileDescriptor_1c26ca21821219d0, []int{1}
}

func (m *Company) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Company.Unmarshal(m, b)
}
func (m *Company) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Company.Marshal(b, m, deterministic)
}
func (m *Company) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Company.Merge(m, src)
}
func (m *Company) XXX_Size() int {
	return xxx_messageInfo_Company.Size(m)
}
func (m *Company) XXX_DiscardUnknown() {
	xxx_messageInfo_Company.DiscardUnknown(m)
}

var xxx_messageInfo_Company proto.InternalMessageInfo

func (m *Company) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Company) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Company) GetWebsite() string {
	if m != nil {
		return m.Website
	}
	return ""
}

func (m *Company) GetPhone() string {
	if m != nil {
		return m.Phone
	}
	return ""
}

func (m *Company) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *Company) GetWhiteLabel() bool {
	if m != nil {
		return m.WhiteLabel
	}
	return false
}

func (m *Company) GetApplicationNotes() string {
	if m != nil {
		return m.ApplicationNotes
	}
	return ""
}

func (m *Company) GetCompanyNumber() string {
	if m != nil {
		return m.CompanyNumber
	}
	return ""
}

func (m *Company) GetCompanyAddress() *CompanyAddress {
	if m != nil {
		return m.CompanyAddress
	}
	return nil
}

func (m *Company) GetState() string {
	if m != nil {
		return m.State
	}
	return ""
}

func (m *Company) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

type SupplierPrintingMethod struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SupplierPrintingMethod) Reset()         { *m = SupplierPrintingMethod{} }
func (m *SupplierPrintingMethod) String() string { return proto.CompactTextString(m) }
func (*SupplierPrintingMethod) ProtoMessage()    {}
func (*SupplierPrintingMethod) Descriptor() ([]byte, []int) {
	return fileDescriptor_1c26ca21821219d0, []int{2}
}

func (m *SupplierPrintingMethod) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SupplierPrintingMethod.Unmarshal(m, b)
}
func (m *SupplierPrintingMethod) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SupplierPrintingMethod.Marshal(b, m, deterministic)
}
func (m *SupplierPrintingMethod) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SupplierPrintingMethod.Merge(m, src)
}
func (m *SupplierPrintingMethod) XXX_Size() int {
	return xxx_messageInfo_SupplierPrintingMethod.Size(m)
}
func (m *SupplierPrintingMethod) XXX_DiscardUnknown() {
	xxx_messageInfo_SupplierPrintingMethod.DiscardUnknown(m)
}

var xxx_messageInfo_SupplierPrintingMethod proto.InternalMessageInfo

func (m *SupplierPrintingMethod) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *SupplierPrintingMethod) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type SupplierFinishingOption struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SupplierFinishingOption) Reset()         { *m = SupplierFinishingOption{} }
func (m *SupplierFinishingOption) String() string { return proto.CompactTextString(m) }
func (*SupplierFinishingOption) ProtoMessage()    {}
func (*SupplierFinishingOption) Descriptor() ([]byte, []int) {
	return fileDescriptor_1c26ca21821219d0, []int{3}
}

func (m *SupplierFinishingOption) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SupplierFinishingOption.Unmarshal(m, b)
}
func (m *SupplierFinishingOption) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SupplierFinishingOption.Marshal(b, m, deterministic)
}
func (m *SupplierFinishingOption) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SupplierFinishingOption.Merge(m, src)
}
func (m *SupplierFinishingOption) XXX_Size() int {
	return xxx_messageInfo_SupplierFinishingOption.Size(m)
}
func (m *SupplierFinishingOption) XXX_DiscardUnknown() {
	xxx_messageInfo_SupplierFinishingOption.DiscardUnknown(m)
}

var xxx_messageInfo_SupplierFinishingOption proto.InternalMessageInfo

func (m *SupplierFinishingOption) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *SupplierFinishingOption) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type SupplierProductCategory struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SupplierProductCategory) Reset()         { *m = SupplierProductCategory{} }
func (m *SupplierProductCategory) String() string { return proto.CompactTextString(m) }
func (*SupplierProductCategory) ProtoMessage()    {}
func (*SupplierProductCategory) Descriptor() ([]byte, []int) {
	return fileDescriptor_1c26ca21821219d0, []int{4}
}

func (m *SupplierProductCategory) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SupplierProductCategory.Unmarshal(m, b)
}
func (m *SupplierProductCategory) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SupplierProductCategory.Marshal(b, m, deterministic)
}
func (m *SupplierProductCategory) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SupplierProductCategory.Merge(m, src)
}
func (m *SupplierProductCategory) XXX_Size() int {
	return xxx_messageInfo_SupplierProductCategory.Size(m)
}
func (m *SupplierProductCategory) XXX_DiscardUnknown() {
	xxx_messageInfo_SupplierProductCategory.DiscardUnknown(m)
}

var xxx_messageInfo_SupplierProductCategory proto.InternalMessageInfo

func (m *SupplierProductCategory) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *SupplierProductCategory) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func init() {
	proto.RegisterType((*CompanyAddress)(nil), "proto.CompanyAddress")
	proto.RegisterType((*Company)(nil), "proto.Company")
	proto.RegisterType((*SupplierPrintingMethod)(nil), "proto.SupplierPrintingMethod")
	proto.RegisterType((*SupplierFinishingOption)(nil), "proto.SupplierFinishingOption")
	proto.RegisterType((*SupplierProductCategory)(nil), "proto.SupplierProductCategory")
}

func init() { proto.RegisterFile("supplier.d.proto", fileDescriptor_1c26ca21821219d0) }

var fileDescriptor_1c26ca21821219d0 = []byte{
	// 389 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x94, 0x93, 0xcf, 0xca, 0xda, 0x40,
	0x14, 0xc5, 0x49, 0xbe, 0x4f, 0x63, 0xae, 0x6d, 0x90, 0xa1, 0x7f, 0x66, 0x51, 0x4a, 0x08, 0xa5,
	0x48, 0x17, 0x42, 0xed, 0xb6, 0x2e, 0xac, 0x50, 0x28, 0x58, 0x2b, 0xe9, 0xce, 0xdd, 0x24, 0x73,
	0x31, 0x03, 0x66, 0x26, 0x4c, 0x26, 0x88, 0x6f, 0xda, 0xc7, 0xe8, 0x23, 0x94, 0x4c, 0x46, 0x34,
	0xba, 0xb1, 0xab, 0xc9, 0xf9, 0xe5, 0x0c, 0xdc, 0x93, 0x73, 0x03, 0x93, 0xba, 0xa9, 0xaa, 0x83,
	0x40, 0x3d, 0xe3, 0xb3, 0x4a, 0x2b, 0xa3, 0xc8, 0xc0, 0x1e, 0xc9, 0x5f, 0x0f, 0xa2, 0x95, 0x2a,
	0x2b, 0x26, 0x4f, 0x4b, 0xce, 0x35, 0xd6, 0x35, 0x89, 0xc0, 0x17, 0x9c, 0x7a, 0xb1, 0x37, 0x7d,
	0x4a, 0x7d, 0xc1, 0xc9, 0x3b, 0x08, 0xf3, 0xce, 0xf1, 0x83, 0x53, 0xdf, 0xe2, 0x0b, 0x20, 0x09,
	0xbc, 0x70, 0x17, 0xd7, 0x42, 0xe2, 0x67, 0xfa, 0x14, 0x7b, 0xd3, 0x30, 0xed, 0xb1, 0x1b, 0xcf,
	0x9c, 0x3e, 0xdf, 0x79, 0xe6, 0x24, 0x86, 0xb1, 0xd3, 0x2b, 0x61, 0x4e, 0x74, 0x60, 0x2d, 0xd7,
	0x88, 0xbc, 0x07, 0x70, 0x72, 0x27, 0x2a, 0x3a, 0xb4, 0x86, 0x2b, 0x42, 0x3e, 0x42, 0x74, 0xb6,
	0xab, 0x46, 0x1a, 0x7d, 0xa2, 0x81, 0xf5, 0xdc, 0xd0, 0xe4, 0x8f, 0x0f, 0x81, 0x8b, 0x7c, 0x97,
	0x95, 0xc0, 0xb3, 0x64, 0x25, 0xda, 0x98, 0x61, 0x6a, 0x9f, 0x09, 0x85, 0xe0, 0x88, 0x59, 0x2d,
	0x0c, 0xba, 0x70, 0x67, 0x49, 0x5e, 0xc1, 0xa0, 0x2a, 0x94, 0x44, 0x17, 0xa8, 0x13, 0x2d, 0xc5,
	0x92, 0x89, 0x83, 0xcb, 0xd0, 0x89, 0x76, 0xfa, 0x63, 0x21, 0x0c, 0xae, 0x59, 0x86, 0x07, 0x3b,
	0xfd, 0x28, 0xbd, 0x22, 0xe4, 0x13, 0x4c, 0x58, 0x5b, 0x51, 0xce, 0x8c, 0x50, 0x72, 0xa3, 0x0c,
	0xd6, 0x6e, 0xfe, 0x3b, 0x4e, 0x3e, 0xc0, 0x4b, 0x57, 0xc0, 0xa6, 0x29, 0x33, 0xd4, 0x74, 0x64,
	0x8d, 0x7d, 0x48, 0x16, 0x10, 0xe5, 0xbd, 0x66, 0x69, 0x18, 0x7b, 0xd3, 0xf1, 0xfc, 0x75, 0xb7,
	0x01, 0xb3, 0x7e, 0xed, 0xe9, 0x8d, 0xb9, 0x8d, 0x51, 0x1b, 0x66, 0x90, 0x42, 0x17, 0xc3, 0x0a,
	0xbb, 0x0c, 0x1a, 0x99, 0x41, 0xbe, 0x34, 0x74, 0x6c, 0xdf, 0x5c, 0x40, 0xf2, 0x15, 0xde, 0xfc,
	0x76, 0x8b, 0xb6, 0xd5, 0x42, 0x1a, 0x21, 0xf7, 0x3f, 0xd1, 0x14, 0x8a, 0x3f, 0xf2, 0xa1, 0x93,
	0x05, 0xbc, 0x3d, 0xdf, 0xfe, 0x2e, 0xa4, 0xa8, 0x0b, 0x21, 0xf7, 0xbf, 0xaa, 0x36, 0xf6, 0xff,
	0x5e, 0xdf, 0x6a, 0xc5, 0x9b, 0xdc, 0xac, 0x98, 0xc1, 0xbd, 0xd2, 0x0f, 0xd5, 0xfc, 0x2d, 0xd8,
	0x75, 0xbf, 0x44, 0x36, 0xb4, 0xc7, 0x97, 0x7f, 0x01, 0x00, 0x00, 0xff, 0xff, 0xa3, 0x4e, 0xbb,
	0xd9, 0x34, 0x03, 0x00, 0x00,
}
