module bitbucket.org/kudouk/kudoproto

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/grpc-ecosystem/grpc-gateway v1.11.1
	github.com/mwitkow/go-proto-validators v0.3.0
	google.golang.org/genproto v0.0.0-20190627203621-eb59cef1c072
	google.golang.org/grpc v1.20.1
)
