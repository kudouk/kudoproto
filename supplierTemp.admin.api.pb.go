// Code generated by protoc-gen-go. DO NOT EDIT.
// source: supplierTemp.admin.api.proto

package proto

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

func init() { proto.RegisterFile("supplierTemp.admin.api.proto", fileDescriptor_4db43c04241fd4ff) }

var fileDescriptor_4db43c04241fd4ff = []byte{
	// 232 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x92, 0x29, 0x2e, 0x2d, 0x28,
	0xc8, 0xc9, 0x4c, 0x2d, 0x0a, 0x49, 0xcd, 0x2d, 0xd0, 0x4b, 0x4c, 0xc9, 0xcd, 0xcc, 0xd3, 0x4b,
	0x2c, 0xc8, 0xd4, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x05, 0x53, 0x52, 0x22, 0x28, 0x8a,
	0x72, 0x21, 0x92, 0x52, 0x32, 0xe9, 0xf9, 0xf9, 0xe9, 0x39, 0xa9, 0xfa, 0x89, 0x05, 0x99, 0xfa,
	0x89, 0x79, 0x79, 0xf9, 0x25, 0x89, 0x25, 0x99, 0xf9, 0x79, 0xc5, 0x10, 0x59, 0xa3, 0xbd, 0x4c,
	0x5c, 0x42, 0xce, 0xf9, 0xb9, 0x05, 0x89, 0x79, 0x95, 0x20, 0x5d, 0xc1, 0xa9, 0x45, 0x65, 0x99,
	0xc9, 0xa9, 0x42, 0xc5, 0x5c, 0x02, 0xee, 0xa9, 0x25, 0x20, 0x11, 0x88, 0x64, 0x66, 0x6a, 0xb1,
	0x90, 0x1c, 0x44, 0x8b, 0x1e, 0xba, 0x44, 0x50, 0x6a, 0x61, 0x69, 0x6a, 0x71, 0x89, 0x94, 0x3c,
	0x4e, 0xf9, 0xe2, 0x82, 0xfc, 0xbc, 0xe2, 0x54, 0x25, 0xb9, 0xa6, 0xcb, 0x4f, 0x26, 0x33, 0x49,
	0x08, 0x89, 0xe9, 0x83, 0x3d, 0x00, 0x76, 0x51, 0x49, 0x6a, 0x6e, 0x41, 0x32, 0xc4, 0x7e, 0xa1,
	0x08, 0x2e, 0x41, 0xe7, 0xa2, 0xd4, 0xc4, 0x92, 0x54, 0x24, 0x07, 0x09, 0xc1, 0x4c, 0xc5, 0x90,
	0x81, 0x59, 0xab, 0x80, 0x5b, 0x01, 0xd4, 0x5e, 0x06, 0x90, 0xc9, 0x61, 0xa9, 0x45, 0x69, 0x99,
	0x95, 0xd8, 0x4c, 0x0e, 0x4b, 0x2d, 0xca, 0x4c, 0xab, 0xc4, 0x63, 0x32, 0x16, 0x05, 0x30, 0x93,
	0x9d, 0xd8, 0xa3, 0x20, 0x81, 0x9f, 0xc4, 0x06, 0xa6, 0x8c, 0x01, 0x01, 0x00, 0x00, 0xff, 0xff,
	0xdf, 0x2e, 0xdd, 0x0c, 0xaa, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// CompanyTempServiceClient is the client API for CompanyTempService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type CompanyTempServiceClient interface {
	GetTempCompanies(ctx context.Context, in *GetTempCompaniesRequest, opts ...grpc.CallOption) (*GetTempCompaniesResponse, error)
	CreateCompanyTemp(ctx context.Context, in *CreateCompanyTempRequest, opts ...grpc.CallOption) (*CreateCompanyTempResponse, error)
	VerfiyCompanyTemp(ctx context.Context, in *VerifyCompanyTempRequest, opts ...grpc.CallOption) (*VerifyCompanyTempResponse, error)
}

type companyTempServiceClient struct {
	cc *grpc.ClientConn
}

func NewCompanyTempServiceClient(cc *grpc.ClientConn) CompanyTempServiceClient {
	return &companyTempServiceClient{cc}
}

func (c *companyTempServiceClient) GetTempCompanies(ctx context.Context, in *GetTempCompaniesRequest, opts ...grpc.CallOption) (*GetTempCompaniesResponse, error) {
	out := new(GetTempCompaniesResponse)
	err := c.cc.Invoke(ctx, "/proto.CompanyTempService/GetTempCompanies", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *companyTempServiceClient) CreateCompanyTemp(ctx context.Context, in *CreateCompanyTempRequest, opts ...grpc.CallOption) (*CreateCompanyTempResponse, error) {
	out := new(CreateCompanyTempResponse)
	err := c.cc.Invoke(ctx, "/proto.CompanyTempService/CreateCompanyTemp", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *companyTempServiceClient) VerfiyCompanyTemp(ctx context.Context, in *VerifyCompanyTempRequest, opts ...grpc.CallOption) (*VerifyCompanyTempResponse, error) {
	out := new(VerifyCompanyTempResponse)
	err := c.cc.Invoke(ctx, "/proto.CompanyTempService/VerfiyCompanyTemp", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CompanyTempServiceServer is the server API for CompanyTempService service.
type CompanyTempServiceServer interface {
	GetTempCompanies(context.Context, *GetTempCompaniesRequest) (*GetTempCompaniesResponse, error)
	CreateCompanyTemp(context.Context, *CreateCompanyTempRequest) (*CreateCompanyTempResponse, error)
	VerfiyCompanyTemp(context.Context, *VerifyCompanyTempRequest) (*VerifyCompanyTempResponse, error)
}

// UnimplementedCompanyTempServiceServer can be embedded to have forward compatible implementations.
type UnimplementedCompanyTempServiceServer struct {
}

func (*UnimplementedCompanyTempServiceServer) GetTempCompanies(ctx context.Context, req *GetTempCompaniesRequest) (*GetTempCompaniesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetTempCompanies not implemented")
}
func (*UnimplementedCompanyTempServiceServer) CreateCompanyTemp(ctx context.Context, req *CreateCompanyTempRequest) (*CreateCompanyTempResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateCompanyTemp not implemented")
}
func (*UnimplementedCompanyTempServiceServer) VerfiyCompanyTemp(ctx context.Context, req *VerifyCompanyTempRequest) (*VerifyCompanyTempResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method VerfiyCompanyTemp not implemented")
}

func RegisterCompanyTempServiceServer(s *grpc.Server, srv CompanyTempServiceServer) {
	s.RegisterService(&_CompanyTempService_serviceDesc, srv)
}

func _CompanyTempService_GetTempCompanies_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetTempCompaniesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CompanyTempServiceServer).GetTempCompanies(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CompanyTempService/GetTempCompanies",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CompanyTempServiceServer).GetTempCompanies(ctx, req.(*GetTempCompaniesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CompanyTempService_CreateCompanyTemp_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateCompanyTempRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CompanyTempServiceServer).CreateCompanyTemp(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CompanyTempService/CreateCompanyTemp",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CompanyTempServiceServer).CreateCompanyTemp(ctx, req.(*CreateCompanyTempRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CompanyTempService_VerfiyCompanyTemp_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(VerifyCompanyTempRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CompanyTempServiceServer).VerfiyCompanyTemp(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CompanyTempService/VerfiyCompanyTemp",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CompanyTempServiceServer).VerfiyCompanyTemp(ctx, req.(*VerifyCompanyTempRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _CompanyTempService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.CompanyTempService",
	HandlerType: (*CompanyTempServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetTempCompanies",
			Handler:    _CompanyTempService_GetTempCompanies_Handler,
		},
		{
			MethodName: "CreateCompanyTemp",
			Handler:    _CompanyTempService_CreateCompanyTemp_Handler,
		},
		{
			MethodName: "VerfiyCompanyTemp",
			Handler:    _CompanyTempService_VerfiyCompanyTemp_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "supplierTemp.admin.api.proto",
}
