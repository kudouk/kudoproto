// Code generated by protoc-gen-go. DO NOT EDIT.
// source: supplier.admin.api.proto

package proto

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

func init() { proto.RegisterFile("supplier.admin.api.proto", fileDescriptor_97b6c5838d9bf8f0) }

var fileDescriptor_97b6c5838d9bf8f0 = []byte{
	// 340 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x84, 0x93, 0xc1, 0x4a, 0xc3, 0x40,
	0x10, 0x86, 0xad, 0x50, 0x85, 0x45, 0xab, 0x4e, 0x11, 0x24, 0x6d, 0xb5, 0xf6, 0x20, 0xa2, 0x90,
	0xa0, 0xde, 0x3c, 0xb6, 0x07, 0x29, 0xe8, 0xa5, 0x22, 0x88, 0xb7, 0xd5, 0x0c, 0x65, 0xa1, 0xd9,
	0x5d, 0xb3, 0x1b, 0x25, 0x88, 0x17, 0x5f, 0xc0, 0x83, 0x2f, 0xe5, 0xdd, 0x57, 0xf0, 0x41, 0xc4,
	0x4d, 0x36, 0x4d, 0x74, 0xa3, 0xa7, 0x0d, 0xf3, 0x0f, 0xdf, 0xff, 0xcf, 0x0c, 0x21, 0x5b, 0x2a,
	0x91, 0x72, 0xc6, 0x30, 0xf6, 0x69, 0x18, 0x31, 0xee, 0x53, 0xc9, 0x7c, 0x19, 0x0b, 0x2d, 0xa0,
	0x69, 0x1e, 0x6f, 0xbd, 0x68, 0x88, 0x32, 0xc1, 0xeb, 0x4e, 0x85, 0x98, 0xce, 0x30, 0xa0, 0x92,
	0x05, 0x94, 0x73, 0xa1, 0xa9, 0x66, 0x82, 0xab, 0x4c, 0x3d, 0x7e, 0x6f, 0x92, 0xd6, 0x48, 0x44,
	0x92, 0xf2, 0xf4, 0x12, 0xe3, 0x07, 0x76, 0x87, 0xf0, 0x48, 0x5a, 0x67, 0xa8, 0xf3, 0xe2, 0x30,
	0x1d, 0x87, 0xd0, 0xcd, 0x9a, 0xfd, 0x6a, 0x79, 0x82, 0xf7, 0x09, 0x2a, 0xed, 0xf5, 0x6a, 0x54,
	0x25, 0x05, 0x57, 0x38, 0xd8, 0x7f, 0xf9, 0xf8, 0x7c, 0x5b, 0x1c, 0x40, 0x3f, 0x30, 0x91, 0x4d,
	0x0e, 0x1b, 0x32, 0x78, 0xb2, 0x5f, 0xe3, 0xf0, 0x19, 0x42, 0xb2, 0x52, 0x30, 0x18, 0x2a, 0xf0,
	0x7e, 0x82, 0x19, 0x2a, 0x6b, 0xda, 0x71, 0x6a, 0xb9, 0x65, 0xc7, 0x58, 0x6e, 0x42, 0xdb, 0x61,
	0x09, 0xaf, 0x0d, 0xd2, 0xbe, 0x92, 0x21, 0xd5, 0x68, 0xe7, 0xd6, 0x54, 0x27, 0x0a, 0x76, 0x73,
	0xa2, 0x43, 0xb3, 0xa6, 0x83, 0xbf, 0x5a, 0x72, 0xef, 0x23, 0xe3, 0x7d, 0xe8, 0xed, 0xfd, 0x37,
	0x6e, 0xa0, 0x34, 0xd5, 0x78, 0xda, 0x38, 0x80, 0x6b, 0xb2, 0x51, 0x1e, 0xe3, 0x7b, 0x7b, 0x0a,
	0x76, 0x1c, 0x03, 0x1a, 0xc5, 0x86, 0xe9, 0xd7, 0x37, 0xe4, 0x51, 0x16, 0xe0, 0x9c, 0xac, 0x8e,
	0x62, 0x9c, 0x67, 0x05, 0xbb, 0xb6, 0x4a, 0xd5, 0x12, 0xbb, 0x6e, 0xb1, 0xa0, 0x8d, 0xcd, 0x7d,
	0x2e, 0x52, 0x0b, 0x2b, 0xdd, 0xa7, 0x28, 0x3a, 0xee, 0x53, 0xd2, 0x0a, 0xd4, 0x84, 0xac, 0x65,
	0x4b, 0x9c, 0xd3, 0x7a, 0x95, 0xe5, 0xfe, 0x02, 0x6e, 0xd7, 0xc9, 0x96, 0x39, 0x5c, 0xbe, 0xc9,
	0xfe, 0x81, 0xdb, 0x25, 0xf3, 0x9c, 0x7c, 0x05, 0x00, 0x00, 0xff, 0xff, 0x61, 0x6f, 0xa7, 0x0b,
	0x2d, 0x03, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// CompanyServiceClient is the client API for CompanyService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type CompanyServiceClient interface {
	GetCompanyById(ctx context.Context, in *GetCompanyByIdRequest, opts ...grpc.CallOption) (*GetCompanyByIdResponse, error)
	GetCompanies(ctx context.Context, in *GetCompaniesRequest, opts ...grpc.CallOption) (*GetCompaniesResponse, error)
	UpdateCompanyStatus(ctx context.Context, in *UpdateCompanyStatusRequest, opts ...grpc.CallOption) (*UpdateCompanyStatusResponse, error)
	GetCompaniesByIds(ctx context.Context, in *GetCompaniesByIdsRequest, opts ...grpc.CallOption) (*GetCompaniesByIdsResponse, error)
	CreateCompany(ctx context.Context, in *CreateCompanyRequest, opts ...grpc.CallOption) (*CreateCompanyResponse, error)
	GetMyCompany(ctx context.Context, in *GetMyCompanyRequest, opts ...grpc.CallOption) (*GetMyCompanyResponse, error)
	UpdateMyCompany(ctx context.Context, in *UpdateMyCompanyRequest, opts ...grpc.CallOption) (*UpdateMyCompanyResponse, error)
}

type companyServiceClient struct {
	cc *grpc.ClientConn
}

func NewCompanyServiceClient(cc *grpc.ClientConn) CompanyServiceClient {
	return &companyServiceClient{cc}
}

func (c *companyServiceClient) GetCompanyById(ctx context.Context, in *GetCompanyByIdRequest, opts ...grpc.CallOption) (*GetCompanyByIdResponse, error) {
	out := new(GetCompanyByIdResponse)
	err := c.cc.Invoke(ctx, "/proto.CompanyService/GetCompanyById", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *companyServiceClient) GetCompanies(ctx context.Context, in *GetCompaniesRequest, opts ...grpc.CallOption) (*GetCompaniesResponse, error) {
	out := new(GetCompaniesResponse)
	err := c.cc.Invoke(ctx, "/proto.CompanyService/GetCompanies", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *companyServiceClient) UpdateCompanyStatus(ctx context.Context, in *UpdateCompanyStatusRequest, opts ...grpc.CallOption) (*UpdateCompanyStatusResponse, error) {
	out := new(UpdateCompanyStatusResponse)
	err := c.cc.Invoke(ctx, "/proto.CompanyService/UpdateCompanyStatus", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *companyServiceClient) GetCompaniesByIds(ctx context.Context, in *GetCompaniesByIdsRequest, opts ...grpc.CallOption) (*GetCompaniesByIdsResponse, error) {
	out := new(GetCompaniesByIdsResponse)
	err := c.cc.Invoke(ctx, "/proto.CompanyService/GetCompaniesByIds", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *companyServiceClient) CreateCompany(ctx context.Context, in *CreateCompanyRequest, opts ...grpc.CallOption) (*CreateCompanyResponse, error) {
	out := new(CreateCompanyResponse)
	err := c.cc.Invoke(ctx, "/proto.CompanyService/CreateCompany", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *companyServiceClient) GetMyCompany(ctx context.Context, in *GetMyCompanyRequest, opts ...grpc.CallOption) (*GetMyCompanyResponse, error) {
	out := new(GetMyCompanyResponse)
	err := c.cc.Invoke(ctx, "/proto.CompanyService/GetMyCompany", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *companyServiceClient) UpdateMyCompany(ctx context.Context, in *UpdateMyCompanyRequest, opts ...grpc.CallOption) (*UpdateMyCompanyResponse, error) {
	out := new(UpdateMyCompanyResponse)
	err := c.cc.Invoke(ctx, "/proto.CompanyService/UpdateMyCompany", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CompanyServiceServer is the server API for CompanyService service.
type CompanyServiceServer interface {
	GetCompanyById(context.Context, *GetCompanyByIdRequest) (*GetCompanyByIdResponse, error)
	GetCompanies(context.Context, *GetCompaniesRequest) (*GetCompaniesResponse, error)
	UpdateCompanyStatus(context.Context, *UpdateCompanyStatusRequest) (*UpdateCompanyStatusResponse, error)
	GetCompaniesByIds(context.Context, *GetCompaniesByIdsRequest) (*GetCompaniesByIdsResponse, error)
	CreateCompany(context.Context, *CreateCompanyRequest) (*CreateCompanyResponse, error)
	GetMyCompany(context.Context, *GetMyCompanyRequest) (*GetMyCompanyResponse, error)
	UpdateMyCompany(context.Context, *UpdateMyCompanyRequest) (*UpdateMyCompanyResponse, error)
}

// UnimplementedCompanyServiceServer can be embedded to have forward compatible implementations.
type UnimplementedCompanyServiceServer struct {
}

func (*UnimplementedCompanyServiceServer) GetCompanyById(ctx context.Context, req *GetCompanyByIdRequest) (*GetCompanyByIdResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCompanyById not implemented")
}
func (*UnimplementedCompanyServiceServer) GetCompanies(ctx context.Context, req *GetCompaniesRequest) (*GetCompaniesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCompanies not implemented")
}
func (*UnimplementedCompanyServiceServer) UpdateCompanyStatus(ctx context.Context, req *UpdateCompanyStatusRequest) (*UpdateCompanyStatusResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateCompanyStatus not implemented")
}
func (*UnimplementedCompanyServiceServer) GetCompaniesByIds(ctx context.Context, req *GetCompaniesByIdsRequest) (*GetCompaniesByIdsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCompaniesByIds not implemented")
}
func (*UnimplementedCompanyServiceServer) CreateCompany(ctx context.Context, req *CreateCompanyRequest) (*CreateCompanyResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateCompany not implemented")
}
func (*UnimplementedCompanyServiceServer) GetMyCompany(ctx context.Context, req *GetMyCompanyRequest) (*GetMyCompanyResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMyCompany not implemented")
}
func (*UnimplementedCompanyServiceServer) UpdateMyCompany(ctx context.Context, req *UpdateMyCompanyRequest) (*UpdateMyCompanyResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateMyCompany not implemented")
}

func RegisterCompanyServiceServer(s *grpc.Server, srv CompanyServiceServer) {
	s.RegisterService(&_CompanyService_serviceDesc, srv)
}

func _CompanyService_GetCompanyById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetCompanyByIdRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CompanyServiceServer).GetCompanyById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CompanyService/GetCompanyById",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CompanyServiceServer).GetCompanyById(ctx, req.(*GetCompanyByIdRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CompanyService_GetCompanies_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetCompaniesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CompanyServiceServer).GetCompanies(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CompanyService/GetCompanies",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CompanyServiceServer).GetCompanies(ctx, req.(*GetCompaniesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CompanyService_UpdateCompanyStatus_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateCompanyStatusRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CompanyServiceServer).UpdateCompanyStatus(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CompanyService/UpdateCompanyStatus",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CompanyServiceServer).UpdateCompanyStatus(ctx, req.(*UpdateCompanyStatusRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CompanyService_GetCompaniesByIds_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetCompaniesByIdsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CompanyServiceServer).GetCompaniesByIds(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CompanyService/GetCompaniesByIds",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CompanyServiceServer).GetCompaniesByIds(ctx, req.(*GetCompaniesByIdsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CompanyService_CreateCompany_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateCompanyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CompanyServiceServer).CreateCompany(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CompanyService/CreateCompany",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CompanyServiceServer).CreateCompany(ctx, req.(*CreateCompanyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CompanyService_GetMyCompany_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetMyCompanyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CompanyServiceServer).GetMyCompany(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CompanyService/GetMyCompany",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CompanyServiceServer).GetMyCompany(ctx, req.(*GetMyCompanyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CompanyService_UpdateMyCompany_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateMyCompanyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CompanyServiceServer).UpdateMyCompany(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CompanyService/UpdateMyCompany",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CompanyServiceServer).UpdateMyCompany(ctx, req.(*UpdateMyCompanyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _CompanyService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.CompanyService",
	HandlerType: (*CompanyServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetCompanyById",
			Handler:    _CompanyService_GetCompanyById_Handler,
		},
		{
			MethodName: "GetCompanies",
			Handler:    _CompanyService_GetCompanies_Handler,
		},
		{
			MethodName: "UpdateCompanyStatus",
			Handler:    _CompanyService_UpdateCompanyStatus_Handler,
		},
		{
			MethodName: "GetCompaniesByIds",
			Handler:    _CompanyService_GetCompaniesByIds_Handler,
		},
		{
			MethodName: "CreateCompany",
			Handler:    _CompanyService_CreateCompany_Handler,
		},
		{
			MethodName: "GetMyCompany",
			Handler:    _CompanyService_GetMyCompany_Handler,
		},
		{
			MethodName: "UpdateMyCompany",
			Handler:    _CompanyService_UpdateMyCompany_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "supplier.admin.api.proto",
}
