VERSION = "unset"
DATE=$(shell date -u +%Y-%m-%d-%H:%M:%S-%Z)
DOCKER_IMAGE ?=	kudo/admin-api-gateway
IMAGE_TAG ?= latest

codegen:
	cp ../../services/kudo-service/proto/**/*.d.proto ./
	cp ../../services/kudo-service/proto/**/*.admin.api.proto ./
	cp ../../services/supplier-service/proto/**/*.d.proto ./
	cp ../../services/supplier-service/proto/**/*.m.proto ./
	cp ../../services/supplier-service/proto/**/*.admin.api.proto ./
	cp ../../services/admin-service/proto/*.proto ./
	cp ../../services/specification-service/proto/**/*.m.proto ./
	cp ../../services/specification-service/proto/**/*.admin.api.proto ./
	cp ../../services/cart-service/proto/**/*.m.proto ./
	cp ../../services/cart-service/proto/**/*.admin.api.proto ./
	cp ../../services/customer-service/proto/**/*.m.proto ./
	cp ../../services/customer-service/proto/**/*.d.proto ./
	cp ../../services/customer-service/proto/**/*.admin.api.proto ./
	cp ../../services/order-service/proto/**/*.m.proto ./
	cp ../../services/order-service/proto/**/*.admin.api.proto ./
	cp ../../services/media-service/proto/**/*.m.proto ./
	cp ../../services/media-service/proto/**/*.admin.api.proto ./
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		-I${GOPATH}/pkg/mod \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		facet.d.proto image.d.proto attribute.admin.api.proto \
		attribute.d.proto collection.d.proto product.d.proto product.admin.api.proto \
		cart.m.proto cart.admin.api.proto \
		supplier.d.proto supplierAdmin.d.proto supplier.m.proto supplier.admin.api.proto \
		supplierAdmin.m.proto supplierAdmin.admin.api.proto \
		supplierTemp.d.proto supplierTemp.m.proto supplierTemp.admin.api.proto \
		admin.proto \
		specification.m.proto task.m.proto tender.m.proto specification.admin.api.proto \
		user.d.proto user.m.proto user.admin.api.proto \
		order.m.proto order.admin.api.proto \
		media.m.proto media.admin.api.proto


kudo:
	cp ../../services/service-service/proto/**/*.d.proto ./
	cp ../../services/service-service/proto/**/*.admin.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		facet.d.proto image.d.proto attribute.admin.api.proto \
		attribute.d.proto collection.d.proto product.d.proto product.admin.api.proto

supplier:
	cp ../../services/supplier-service/proto/**/*.d.proto ./
	cp ../../services/supplier-service/proto/**/*.m.proto ./
	cp ../../services/supplier-service/proto/**/*.admin.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		supplier.d.proto supplierAdmin.d.proto supplier.m.proto supplier.admin.api.proto \
		supplierAdmin.m.proto supplierAdmin.admin.api.proto \
		supplierTemp.d.proto supplierTemp.m.proto supplierTemp.admin.api.proto

admin:
	cp ../../services/admin-service-service/proto/*.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		admin.proto

specification:
	cp ../../services/specification-service/proto/**/*.m.proto ./
	cp ../../services/specification-service/proto/**/*.admin.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		specification.m.proto task.m.proto tender.m.proto specification.admin.api.proto

cart:
	cp ../../services/cart-service/proto/**/*.m.proto ./
	cp ../../services/cart-service/proto/**/*.admin.api.proto ./
	# GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	# GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	# go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		-I${GOPATH}/pkg/mod \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		image.d.proto attribute.d.proto cart.m.proto cart.admin.api.proto

customer:
	cp ../../services/customer-service/proto/**/*.m.proto ./
	cp ../../services/customer-service/proto/**/*.d.proto ./
	cp ../../services/customer-service/proto/**/*.admin.api.proto ./
	# GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	# GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	# go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		-I${GOPATH}/pkg/mod \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		user.d.proto user.m.proto user.admin.api.proto

order:
	cp ../../services/orders-service/proto/**/*.m.proto ./
	cp ../../services/orders-service/proto/**/*.admin.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		cart.m.proto order.m.proto order.admin.api.proto

media:
	cp ../../services/media-service/proto/**/*.m.proto ./
	cp ../../services/media-service/proto/**/*.admin.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		media.m.proto media.admin.api.proto

# codegen:
# 	cp ../../services/service-service/proto/**/*.proto ./
# 	cp ../admin-service-service/proto/*.proto ./
# 	cp ../../services/suppliers-service/proto/*.proto ./
# 	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
# 	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
# 	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
# 	protoc -I/usr/local/include -I. \
# 		-I${GOPATH}/src \
# 		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
# 		--grpc-gateway_out=logtostderr=true:. \
# 		--swagger_out=logtostderr=true:. \
# 		--go_out=plugins=grpc:. \
# 		--govalidators_out=. \
# 		attribute.proto collection.proto facet.proto product.proto
# 	protoc -I/usr/local/include -I. \
# 		-I${GOPATH}/src \
# 		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
# 		--grpc-gateway_out=logtostderr=true:. \
# 		--swagger_out=logtostderr=true:. \
# 		--go_out=plugins=grpc:. \
# 		--govalidators_out=. \
# 		admin.proto
# 	protoc -I/usr/local/include -I. \
# 		-I${GOPATH}/src \
# 		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
# 		--grpc-gateway_out=logtostderr=true:. \
# 		--swagger_out=logtostderr=true:. \
# 		--go_out=plugins=grpc:. \
# 		--govalidators_out=. \
# 		supplierTemp.proto supplierAdmin.proto supplier.proto
