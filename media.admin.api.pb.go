// Code generated by protoc-gen-go. DO NOT EDIT.
// source: media.admin.api.proto

package proto

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

func init() { proto.RegisterFile("media.admin.api.proto", fileDescriptor_cdf1ead93b795504) }

var fileDescriptor_cdf1ead93b795504 = []byte{
	// 296 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0xcd, 0x4d, 0x4d, 0xc9,
	0x4c, 0xd4, 0x4b, 0x4c, 0xc9, 0xcd, 0xcc, 0xd3, 0x4b, 0x2c, 0xc8, 0xd4, 0x2b, 0x28, 0xca, 0x2f,
	0xc9, 0x17, 0x62, 0x05, 0x53, 0x52, 0xbc, 0x10, 0xd9, 0x5c, 0x88, 0xa8, 0x94, 0x4c, 0x7a, 0x7e,
	0x7e, 0x7a, 0x4e, 0xaa, 0x7e, 0x62, 0x41, 0xa6, 0x7e, 0x62, 0x5e, 0x5e, 0x7e, 0x49, 0x62, 0x49,
	0x66, 0x7e, 0x5e, 0x31, 0x44, 0xd6, 0x68, 0x0a, 0x0b, 0x17, 0x8f, 0x2f, 0x48, 0x7d, 0x70, 0x6a,
	0x51, 0x59, 0x66, 0x72, 0xaa, 0x50, 0x01, 0x17, 0x8f, 0x7b, 0x6a, 0x49, 0x68, 0x41, 0x4e, 0x7e,
	0x62, 0x4a, 0x68, 0x51, 0x8e, 0x90, 0x14, 0x44, 0xa1, 0x1e, 0xb2, 0x60, 0x50, 0x6a, 0x61, 0x69,
	0x6a, 0x71, 0x89, 0x94, 0x34, 0x56, 0xb9, 0xe2, 0x82, 0xfc, 0xbc, 0xe2, 0x54, 0x25, 0xf5, 0xa6,
	0xcb, 0x4f, 0x26, 0x33, 0x29, 0x2a, 0xc9, 0xe8, 0x83, 0xdd, 0x09, 0xb6, 0x1f, 0xec, 0x32, 0xfd,
	0xea, 0xcc, 0x94, 0x5a, 0xfd, 0x52, 0xb0, 0x06, 0x2b, 0x46, 0x2d, 0xa1, 0x5c, 0x2e, 0x7e, 0xe7,
	0xa2, 0xd4, 0xc4, 0x92, 0x54, 0xb0, 0x3b, 0x3c, 0x4b, 0x52, 0x73, 0x85, 0x64, 0xa1, 0x06, 0xa3,
	0x89, 0xc3, 0xec, 0x95, 0xc3, 0x25, 0x0d, 0xb5, 0x5a, 0x1a, 0x6c, 0xb5, 0xa8, 0x92, 0x00, 0xba,
	0xd5, 0x20, 0xeb, 0x82, 0xb8, 0xf8, 0x43, 0x0b, 0x52, 0xb0, 0x5a, 0x87, 0x26, 0x8e, 0x6e, 0x1d,
	0x86, 0x34, 0xd4, 0x3a, 0x06, 0xa1, 0x22, 0x2e, 0x01, 0xf7, 0xd4, 0x12, 0xb8, 0x8c, 0x53, 0xa5,
	0xa7, 0x8b, 0x90, 0x1c, 0x22, 0x70, 0x50, 0x24, 0x60, 0xa6, 0xca, 0xe3, 0x94, 0x87, 0x1a, 0x2b,
	0x0b, 0xf6, 0x85, 0xb8, 0x90, 0x28, 0xd6, 0x00, 0x14, 0x8a, 0xe6, 0x12, 0x42, 0xd6, 0x5a, 0x0c,
	0xd2, 0x5b, 0x2c, 0xa4, 0x80, 0xc5, 0x54, 0x88, 0x14, 0xcc, 0x5e, 0x45, 0x3c, 0x2a, 0x60, 0x1e,
	0x72, 0x62, 0x8f, 0x82, 0x24, 0xa6, 0x24, 0x36, 0x30, 0x65, 0x0c, 0x08, 0x00, 0x00, 0xff, 0xff,
	0x9d, 0xf0, 0x7c, 0x2a, 0x73, 0x02, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// MediaServiceClient is the client API for MediaService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type MediaServiceClient interface {
	GetUploadUrl(ctx context.Context, in *GetUploadUrlRequest, opts ...grpc.CallOption) (*GetUploadUrlResponse, error)
	CreateMediaItem(ctx context.Context, in *CreateMediaItemRequest, opts ...grpc.CallOption) (*CreateMediaItemResponse, error)
	UpdateMediaItem(ctx context.Context, in *UpdateMediaItemRequest, opts ...grpc.CallOption) (*UpdateMediaItemResponse, error)
	GetMediaItemByID(ctx context.Context, in *GetMediaItemByIDRequest, opts ...grpc.CallOption) (*GetMediaItemByIDResponse, error)
	GetMediaItemsByIDs(ctx context.Context, in *GetMediaItemsByIDsRequest, opts ...grpc.CallOption) (*GetMediaItemsByIDsResponse, error)
}

type mediaServiceClient struct {
	cc *grpc.ClientConn
}

func NewMediaServiceClient(cc *grpc.ClientConn) MediaServiceClient {
	return &mediaServiceClient{cc}
}

func (c *mediaServiceClient) GetUploadUrl(ctx context.Context, in *GetUploadUrlRequest, opts ...grpc.CallOption) (*GetUploadUrlResponse, error) {
	out := new(GetUploadUrlResponse)
	err := c.cc.Invoke(ctx, "/proto.MediaService/GetUploadUrl", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mediaServiceClient) CreateMediaItem(ctx context.Context, in *CreateMediaItemRequest, opts ...grpc.CallOption) (*CreateMediaItemResponse, error) {
	out := new(CreateMediaItemResponse)
	err := c.cc.Invoke(ctx, "/proto.MediaService/CreateMediaItem", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mediaServiceClient) UpdateMediaItem(ctx context.Context, in *UpdateMediaItemRequest, opts ...grpc.CallOption) (*UpdateMediaItemResponse, error) {
	out := new(UpdateMediaItemResponse)
	err := c.cc.Invoke(ctx, "/proto.MediaService/UpdateMediaItem", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mediaServiceClient) GetMediaItemByID(ctx context.Context, in *GetMediaItemByIDRequest, opts ...grpc.CallOption) (*GetMediaItemByIDResponse, error) {
	out := new(GetMediaItemByIDResponse)
	err := c.cc.Invoke(ctx, "/proto.MediaService/GetMediaItemByID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mediaServiceClient) GetMediaItemsByIDs(ctx context.Context, in *GetMediaItemsByIDsRequest, opts ...grpc.CallOption) (*GetMediaItemsByIDsResponse, error) {
	out := new(GetMediaItemsByIDsResponse)
	err := c.cc.Invoke(ctx, "/proto.MediaService/GetMediaItemsByIDs", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MediaServiceServer is the server API for MediaService service.
type MediaServiceServer interface {
	GetUploadUrl(context.Context, *GetUploadUrlRequest) (*GetUploadUrlResponse, error)
	CreateMediaItem(context.Context, *CreateMediaItemRequest) (*CreateMediaItemResponse, error)
	UpdateMediaItem(context.Context, *UpdateMediaItemRequest) (*UpdateMediaItemResponse, error)
	GetMediaItemByID(context.Context, *GetMediaItemByIDRequest) (*GetMediaItemByIDResponse, error)
	GetMediaItemsByIDs(context.Context, *GetMediaItemsByIDsRequest) (*GetMediaItemsByIDsResponse, error)
}

// UnimplementedMediaServiceServer can be embedded to have forward compatible implementations.
type UnimplementedMediaServiceServer struct {
}

func (*UnimplementedMediaServiceServer) GetUploadUrl(ctx context.Context, req *GetUploadUrlRequest) (*GetUploadUrlResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetUploadUrl not implemented")
}
func (*UnimplementedMediaServiceServer) CreateMediaItem(ctx context.Context, req *CreateMediaItemRequest) (*CreateMediaItemResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateMediaItem not implemented")
}
func (*UnimplementedMediaServiceServer) UpdateMediaItem(ctx context.Context, req *UpdateMediaItemRequest) (*UpdateMediaItemResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateMediaItem not implemented")
}
func (*UnimplementedMediaServiceServer) GetMediaItemByID(ctx context.Context, req *GetMediaItemByIDRequest) (*GetMediaItemByIDResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMediaItemByID not implemented")
}
func (*UnimplementedMediaServiceServer) GetMediaItemsByIDs(ctx context.Context, req *GetMediaItemsByIDsRequest) (*GetMediaItemsByIDsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMediaItemsByIDs not implemented")
}

func RegisterMediaServiceServer(s *grpc.Server, srv MediaServiceServer) {
	s.RegisterService(&_MediaService_serviceDesc, srv)
}

func _MediaService_GetUploadUrl_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetUploadUrlRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MediaServiceServer).GetUploadUrl(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.MediaService/GetUploadUrl",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MediaServiceServer).GetUploadUrl(ctx, req.(*GetUploadUrlRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MediaService_CreateMediaItem_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateMediaItemRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MediaServiceServer).CreateMediaItem(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.MediaService/CreateMediaItem",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MediaServiceServer).CreateMediaItem(ctx, req.(*CreateMediaItemRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MediaService_UpdateMediaItem_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateMediaItemRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MediaServiceServer).UpdateMediaItem(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.MediaService/UpdateMediaItem",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MediaServiceServer).UpdateMediaItem(ctx, req.(*UpdateMediaItemRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MediaService_GetMediaItemByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetMediaItemByIDRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MediaServiceServer).GetMediaItemByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.MediaService/GetMediaItemByID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MediaServiceServer).GetMediaItemByID(ctx, req.(*GetMediaItemByIDRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MediaService_GetMediaItemsByIDs_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetMediaItemsByIDsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MediaServiceServer).GetMediaItemsByIDs(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.MediaService/GetMediaItemsByIDs",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MediaServiceServer).GetMediaItemsByIDs(ctx, req.(*GetMediaItemsByIDsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _MediaService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.MediaService",
	HandlerType: (*MediaServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetUploadUrl",
			Handler:    _MediaService_GetUploadUrl_Handler,
		},
		{
			MethodName: "CreateMediaItem",
			Handler:    _MediaService_CreateMediaItem_Handler,
		},
		{
			MethodName: "UpdateMediaItem",
			Handler:    _MediaService_UpdateMediaItem_Handler,
		},
		{
			MethodName: "GetMediaItemByID",
			Handler:    _MediaService_GetMediaItemByID_Handler,
		},
		{
			MethodName: "GetMediaItemsByIDs",
			Handler:    _MediaService_GetMediaItemsByIDs_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "media.admin.api.proto",
}
